//******************************************************************************
// SCICHART® Copyright SciChart Ltd. 2011-2017. All rights reserved.
//
// Web: http://www.scichart.com
// Support: support@scichart.com
// Sales:   sales@scichart.com
//
// SciChartApp.java is part of the SCICHART® Examples. Permission is hereby granted
// to modify, create derivative works, distribute and publish any part of this source
// code whether for commercial, private or personal use.
//
// The SCICHART® examples are distributed in the hope that they will be useful, but
// without any warranty. It is provided "AS IS" without warranty of any kind, either
// expressed or implied.
//******************************************************************************

package com.scichart.examples;

import android.app.Application;
import android.util.Log;

//BEGIN_DEMO_APPLICATION
import com.scichart.examples.demo.helpers.Module;
import com.scichart.examples.demo.search.ExampleSearchProvider;
//END_DEMO_APPLICATION

public class SciChartApp extends Application {

    private static SciChartApp sInstance;

    public static SciChartApp getInstance() {
        return sInstance;
    }

    //BEGIN_DEMO_APPLICATION
    private Module module;
    private ExampleSearchProvider searchProvider;
    //END_DEMO_APPLICATION

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        try {
            setUpSciChartLicense();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpSciChartLicense() throws Exception {
        // Set your license code here to license the SciChart Android Examples app
        // You can get a trial license key from https://www.scichart.com/licensing-scichart-android/
        // Purchased license keys can be viewed at https://www.scichart.com/profile
        //
        // e.g.
        //
         com.scichart.charting.visuals.SciChartSurface.setRuntimeLicenseKey(
                "<LicenseContract>" +
                        "<Customer>deekshasinghal3@gmail.com</Customer>" +
                        "<OrderId>Trial</OrderId>" +
                        "<LicenseCount>1</LicenseCount>" +
                        "<IsTrialLicense>true</IsTrialLicense>" +
                        "<SupportExpires>02/04/2019 00:00:00</SupportExpires>" +
                        "<ProductCode>SC-ANDROID-2D-ENTERPRISE-SRC</ProductCode>" +
                        "<KeyCode>6ccc960b22b7b12360a141a7c2a89bce4e40.....09744b6c195022e9fa1ebcf9a0e78167cbaa8f9b8eee9221b4263548f9182a1e9f96828084f9a338129bc8c2bdfadb256686b41abad4ee333ad4f25b84b98de3f63874352f08736f7557a55d9cd902820a980c8b14df64dfb472bf637aa13a1ed099a1a019eee53210679e21e7de250de2d3071639104952c1f9fb55e302c089b1547f61ef418a425510a5980e845a65dd39ec5c2a75ed23faceede74f0f1dc9b72bcf7411eb7bcb3441ab00eec76385a023f09ac592bf5b97a7832aec1c716324f9c79828a91c62422962</KeyCode>" +
                "</LicenseContract>"
         );

        /*try {
            com.scichart.charting.visuals.SciChartSurface.setRuntimeLicenseKey("");
        } catch (Exception e) {
            Log.e("SciChart", "Error when setting the license", e);
        }*/
    }

    //BEGIN_DEMO_APPLICATION
    public Module getModule() {
        if (module == null) {
            module = initModule();
        }
        return module;
    }

    public ExampleSearchProvider getSearchProvider(Module module) {
        if (searchProvider == null) {
            searchProvider = initSearchProvider(module);
        }
        return searchProvider;
    }

    private Module initModule() {
        final Module module = new Module(getApplicationContext());
        module.initialize();
        return module;
    }

    private ExampleSearchProvider initSearchProvider(Module module) {
        if (module != null) {
            return new ExampleSearchProvider(getApplicationContext(), module.getExamples());
        }
        return null;
    }
    //END_DEMO_APPLICATION
}
